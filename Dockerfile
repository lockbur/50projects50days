FROM plfmv1h0.mirror.aliyuncs.com/library/nginx:1.21.1-alpine

# Copy Nginx Setting
COPY nginx.conf /etc/nginx/nginx.conf

# Copy APP Files
COPY dist/ /data/app
COPY projects/ /data/app/projects
