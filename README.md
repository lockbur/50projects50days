# 50天50项目 - HTML/CSS and JavaScript

50个迷你前端项目,提升自己的HTML, CSS & JavaScript技能

-   [50天50项目](https://50projects50days.cn)
-   [50天50项目代码仓库](https://gitee.com/lockbur/50projects50days)

|  #  | Project                                                                                                                     | Live Demo                                                                         |
| :-: | --------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- |
| 01  | [Expanding Cards](https://gitee.com/lockbur/50projects50days/tree/master/projects/expanding-cards)                             | [Live Demo](https://50projects50days.cn/projects/expanding-cards/)               |
| 02  | [Progress Steps](https://gitee.com/lockbur/50projects50days/tree/master/projects/progress-steps)                               | [Live Demo](https://50projects50days.cn/projects/progress-steps/)                |
| 03  | [Rotating Navigation Animation](https://gitee.com/lockbur/50projects50days/tree/master/projects/rotating-nav-animation)                       | [Live Demo](https://50projects50days.cn/projects/rotating-navigation-animation/) |
| 04  | [Hidden Search Widget](https://gitee.com/lockbur/50projects50days/tree/master/projects/hidden-search)                          | [Live Demo](https://50projects50days.cn/projects/hidden-search-widget/)          |
| 05  | [Blurry Loading](https://gitee.com/lockbur/50projects50days/tree/master/projects/blurry-loading)                               | [Live Demo](https://50projects50days.cn/projects/blurry-loading/)                |
| 06  | [Scroll Animation](https://gitee.com/lockbur/50projects50days/tree/master/projects/scroll-animation)                           | [Live Demo](https://50projects50days.cn/projects/scroll-animation/)              |
| 07  | [Split Landing Page](https://gitee.com/lockbur/50projects50days/tree/master/projects/split-landing-page)                       | [Live Demo](https://50projects50days.cn/projects/split-landing-page/)            |
| 08  | [Form Wave](https://gitee.com/lockbur/50projects50days/tree/master/projects/form-input-wave)                                         | [Live Demo](https://50projects50days.cn/projects/form-wave/)                     |
| 09  | [Sound Board](https://gitee.com/lockbur/50projects50days/tree/master/projects/sound-board)                                     | [Live Demo](https://50projects50days.cn/projects/sound-board/)                   |
| 10  | [Dad Jokes](https://gitee.com/lockbur/50projects50days/tree/master/projects/dad-jokes)                                         | [Live Demo](https://50projects50days.cn/projects/dad-jokes/)                     |
| 11  | [Event Keycodes](https://gitee.com/lockbur/50projects50days/tree/master/projects/event-keycodes)                               | [Live Demo](https://50projects50days.cn/projects/event-keycodes/)                |
| 12  | [Faq Collapse](https://gitee.com/lockbur/50projects50days/tree/master/projects/faq-collapse)                                   | [Live Demo](https://50projects50days.cn/projects/faq-collapse/)                  |
| 13  | [Random Choice Picker](https://gitee.com/lockbur/50projects50days/tree/master/projects/random-choice-picker)                   | [Live Demo](https://50projects50days.cn/projects/random-choice-picker/)          |
| 14  | [Animated Navigation](https://gitee.com/lockbur/50projects50days/tree/master/projects/animated-navigation)                     | [Live Demo](https://50projects50days.cn/projects/animated-navigation/)           |
| 15  | [Incrementing Counter](https://gitee.com/lockbur/50projects50days/tree/master/projects/incrementing-counter)                   | [Live Demo](https://50projects50days.cn/projects/incrementing-counter/)          |
| 16  | [Drink Water](https://gitee.com/lockbur/50projects50days/tree/master/projects/drink-water)                                     | [Live Demo](https://50projects50days.cn/projects/drink-water/)                   |
| 17  | [Movie App](https://gitee.com/lockbur/50projects50days/tree/master/projects/movie-app)                                         | [Live Demo](https://50projects50days.cn/projects/movie-app/)                     |
| 18  | [Background Slider](https://gitee.com/lockbur/50projects50days/tree/master/projects/background-slider)                         | [Live Demo](https://50projects50days.cn/projects/background-slider/)             |
| 19  | [Theme Clock](https://gitee.com/lockbur/50projects50days/tree/master/projects/theme-clock)                                     | [Live Demo](https://50projects50days.cn/projects/theme-clock/)                   |
| 20  | [Button Ripple Effect](https://gitee.com/lockbur/50projects50days/tree/master/projects/button-ripple-effect)                   | [Live Demo](https://50projects50days.cn/projects/button-ripple-effect/)          |
| 21  | [Drag N Drop](https://gitee.com/lockbur/50projects50days/tree/master/projects/drag-n-drop)                                     | [Live Demo](https://50projects50days.cn/projects/drag-n-drop/)                   |
| 22  | [Drawing App](https://gitee.com/lockbur/50projects50days/tree/master/projects/drawing-app)                                     | [Live Demo](https://50projects50days.cn/projects/drawing-app/)                   |
| 23  | [Kinetic Loader](https://gitee.com/lockbur/50projects50days/tree/master/projects/kinetic-loader)                               | [Live Demo](https://50projects50days.cn/projects/kinetic-loader/)                |
| 24  | [Content Placeholder](https://gitee.com/lockbur/50projects50days/tree/master/projects/content-placeholder)                     | [Live Demo](https://50projects50days.cn/projects/content-placeholder/)           |
| 25  | [Sticky Navbar](https://gitee.com/lockbur/50projects50days/tree/master/projects/sticky-navigation)                                 | [Live Demo](https://50projects50days.cn/projects/sticky-navbar/)                 |
| 26  | [Double Vertical Slider](https://gitee.com/lockbur/50projects50days/tree/master/projects/double-vertical-slider)               | [Live Demo](https://50projects50days.cn/projects/double-vertical-slider/)        |
| 27  | [Toast Notification](https://gitee.com/lockbur/50projects50days/tree/master/projects/toast-notification)                       | [Live Demo](https://50projects50days.cn/projects/toast-notification/)            |
| 28  | [Github Profiles](https://gitee.com/lockbur/50projects50days/tree/master/projects/github-profiles)                             | [Live Demo](https://50projects50days.cn/projects/github-profiles/)               |
| 29  | [Double Click Heart](https://gitee.com/lockbur/50projects50days/tree/master/projects/double-click-heart)                       | [Live Demo](https://50projects50days.cn/projects/double-click-heart/)            |
| 30  | [Auto Text Effect](https://gitee.com/lockbur/50projects50days/tree/master/projects/auto-text-effect)                           | [Live Demo](https://50projects50days.cn/projects/auto-text-effect/)              |
| 31  | [Password Generator](https://gitee.com/lockbur/50projects50days/tree/master/projects/password-generator)                       | [Live Demo](https://50projects50days.cn/projects/password-generator/)            |
| 32  | [Good Cheap Fast](https://gitee.com/lockbur/50projects50days/tree/master/projects/good-cheap-fast)                             | [Live Demo](https://50projects50days.cn/projects/good-cheap-fast/)               |
| 33  | [Notes App](https://gitee.com/lockbur/50projects50days/tree/master/projects/notes-app)                                         | [Live Demo](https://50projects50days.cn/projects/notes-app/)                     |
| 34  | [Animated Countdown](https://gitee.com/lockbur/50projects50days/tree/master/projects/animated-countdown)                       | [Live Demo](https://50projects50days.cn/projects/animated-countdown/)            |
| 35  | [Image Carousel](https://gitee.com/lockbur/50projects50days/tree/master/projects/image-carousel)                               | [Live Demo](https://50projects50days.cn/projects/image-carousel/)                |
| 36  | [Hoverboard](https://gitee.com/lockbur/50projects50days/tree/master/projects/hoverboard)                                       | [Live Demo](https://50projects50days.cn/projects/hoverboard/)                    |
| 37  | [Pokedex](https://gitee.com/lockbur/50projects50days/tree/master/projects/pokedex)                                             | [Live Demo](https://50projects50days.cn/projects/pokedex/)                       |
| 38  | [Mobile Tab Navigation](https://gitee.com/lockbur/50projects50days/tree/master/projects/mobile-tab-navigation)                 | [Live Demo](https://50projects50days.cn/projects/mobile-tab-navigation/)         |
| 39  | [Password Strength Background](https://gitee.com/lockbur/50projects50days/tree/master/projects/password-strength-background)   | [Live Demo](https://50projects50days.cn/projects/password-strength-background/)  |
| 40  | [3d Background Boxes](https://gitee.com/lockbur/50projects50days/tree/master/projects/3d-boxes-background)                     | [Live Demo](https://50projects50days.cn/projects/3d-background-boxes/)           |
| 41  | [Verify Account Ui](https://gitee.com/lockbur/50projects50days/tree/master/projects/verify-account-ui)                         | [Live Demo](https://50projects50days.cn/projects/verify-account-ui/)             |
| 42  | [Live User Filter](https://gitee.com/lockbur/50projects50days/tree/master/projects/live-user-filter)                           | [Live Demo](https://50projects50days.cn/projects/live-user-filter/)              |
| 43  | [Feedback Ui Design](https://gitee.com/lockbur/50projects50days/tree/master/projects/feedback-ui-design)                       | [Live Demo](https://50projects50days.cn/projects/feedback-ui-design/)            |
| 44  | [Custom Range Slider](https://gitee.com/lockbur/50projects50days/tree/master/projects/custom-range-slider)                     | [Live Demo](https://50projects50days.cn/projects/custom-range-slider/)           |
| 45  | [Netflix Mobile Navigation](https://gitee.com/lockbur/50projects50days/tree/master/projects/netflix-mobile-navigation)         | [Live Demo](https://50projects50days.cn/projects/netflix-mobile-navigation/)     |
| 46  | [Quiz App](https://gitee.com/lockbur/50projects50days/tree/master/projects/quiz-app)                                           | [Live Demo](https://50projects50days.cn/projects/quiz-app/)                      |
| 47  | [Testimonial Box Switcher](https://gitee.com/lockbur/50projects50days/tree/master/projects/testimonial-box-switcher)           | [Live Demo](https://50projects50days.cn/projects/testimonial-box-switcher/)      |
| 48  | [Random Image Feed](https://gitee.com/lockbur/50projects50days/tree/master/projects/random-image-generator)                         | [Live Demo](https://50projects50days.cn/projects/random-image-feed/)             |
| 49  | [Todo List](https://gitee.com/lockbur/50projects50days/tree/master/projects/todo-list)                                         | [Live Demo](https://50projects50days.cn/projects/todo-list/)                     |
| 50  | [Insect Catch Game](https://gitee.com/lockbur/50projects50days/tree/master/projects/insect-catch-game)                         | [Live Demo](https://50projects50days.cn/projects/insect-catch-game/)             |

**NOTE ON PULL REQUESTS**: All of these projects are part of the course. While I do appreciate people trying to make some things prettier or adding new features, we are only accepting pull requests and looking at issues for bug fixes so that the code stays inline with the course

## License

The MIT License

Copyright (c) 2020-2021 Traversy Media https://traversymedia.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

