import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Main',
        component: () => import("../views/layout/main"),
        redirect: '/',
        children: [
            {
                path: '',
                name: 'projects',
                component: () => import('@/views/projects')
            },
            {
                path: 'projects',
                name: 'projects',
                component: () => import('@/views/projects')
            },
            {
                path: 'about',
                name: 'about',
                component: () => import('@/views/about')
            }
        ]
    },
    {
        path: '/expanding-cards',
        name: 'expanding-cards',
        component: () => import("../views/vue/expanding-cards")
    },
    {
        path: '/progress-steps',
        name: 'progress-steps',
        component: () => import("../views/vue/progress-steps")
    },
    {
        path: '/rotating-nav-animation',
        name: 'rotating-nav-animation',
        component: () => import("../views/vue/rotating-nav-animation")
    },
    {
        path: '/404',
        name: '404',
        component: () => import('@/views/404')
    },
    {
        path: '403',
        name: '403',
        component: () => import('@/views/403')
    },
    {
        path: '*',
        name: '404',
        redirect: '/404'
    }
]
const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes
})
router.beforeEach(async (to, from, next) => {
    if (to.path) {
        if (window._hmt) {
            window._hmt.push(['_trackPageview', '/#' + to.fullPath]);
        }
    }
    next();
})
export default router
