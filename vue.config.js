const path = require('path')
module.exports = {
    lintOnSave: false,
    devServer: {
        port: 9000
    },
    configureWebpack: {
        externals: {
            "vue": "Vue",
            "vuex": "Vuex",
            "vue-router": "VueRouter"
        }
    },
    chainWebpack: config => {
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
        types.forEach(type => addStyleResource(config.module.rule('less').oneOf(type)));
    }
}

function addStyleResource(rule) {
    rule.use('style-resource')
        .loader('style-resources-loader')
        .options({patterns: []})
}
